import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { PostService } from "src/app/services/post.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  name: any;
  post: any;
  listUser: any;

  constructor(
    private router: Router,
    private postService: PostService,
  ) { }

  ngOnInit(){
    this.name = JSON.parse(sessionStorage.getItem('profile')).username;
    this.post = this.postService.listPost
    this.listUser = JSON.parse(sessionStorage.getItem('listUser'))
    for (let i = 0; i < this.post.length; i++) {
      for (let j = 0; j < this.listUser.length; j++){
        if(this.post[i].userId == this.listUser[j].id){
          this.post[i].userId = this.listUser[j].username
        }
      }
    }
  }

  logout(){
    sessionStorage.clear()
    this.router.navigate(['/home'])
  }

  detail(data){
    sessionStorage.setItem('posting-detail', JSON.stringify(data))
    this.router.navigate(['/post'])
  }
}
