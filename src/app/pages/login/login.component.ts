import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hasil: any;
  formLogin: FormGroup;
  status: any;
  message: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.hasil = this.authService.listUser
    
    this.formLogin = this.formBuilder.group({
      username: new FormControl(null, []),
      password: new FormControl(null, []),
    });
  }

  onClickSubmit() {
      for (let i = 0; i < this.hasil.length; i++) 
      {
        if (this.hasil[i].username == this.formLogin.get('username').value){
          console.log(this.hasil[i].username, this.formLogin.get('username').value)
          if (this.formLogin.get('username').value == this.formLogin.get('password').value){
            sessionStorage.setItem('profile', JSON.stringify(this.hasil[i]))
            sessionStorage.setItem('listUser', JSON.stringify(this.hasil))
            this.router.navigate(['/dashboard'])
          }
          else{
            this.status = false;
            this.message = 'Username/password Anda salah';
          }
          break
        }
        else{
          this.status = false;
          this.message = 'Akun/username belum terdaftar';
        }
      }
  }
}

