import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  name: any;
  post: any;

  constructor(
    private router: Router,

  ) { }

  ngOnInit(){
    this.name = JSON.parse(sessionStorage.getItem('profile')).username;
    this.post = JSON.parse(sessionStorage.getItem('posting-detail'))
  }
  logout(){
    sessionStorage.clear()
    this.router.navigate(['/home'])
  }
}
