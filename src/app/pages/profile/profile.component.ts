import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  name: any;
  profile: any;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(){
    this.name = JSON.parse(sessionStorage.getItem('profile')).username;
    this.profile = JSON.parse(sessionStorage.getItem('profile'));
 }

 logout(){
   sessionStorage.clear()
   this.router.navigate(['/home'])
 }
}
